const serveOrderWithSameDrone = true;
// const serveOrderWithSameDrone = false;

class Warehouse {
  constructor(id, x, y) {
    this.id = id;
    this.location = { x, y };
  }
}

class Customer {
  constructor(id, x, y) {
    this.id = id;
    this.location = { x, y };
    this.orderId = [];
  }
}

class Order {
  constructor(id) {
    this.id = id;
    this.customerId;
    this.products;
  }
}

class Drone {
  constructor(id) {
    this.id = id;
  }
}

class DroneDeliveryNetwork {
  constructor(warehouses, customers, orders) {
    this.warehouses = warehouses;
    this.customers = customers;
    this.orders = orders;
  }

  calculateTotalTime() {
    const orderWithCustomerLocation = this.orders.map((order) => {
      const customer = this.customers.find(
        (customer) => customer.id === order.customerId
      );
      const customerLocation = customer.location;
      return { ...order, customerLocation };
    });

    const orderWithDistances = orderWithCustomerLocation.map((order) => {
      const distance = this.findNearestWarehouse(order.customerLocation);
      return { ...order, distance };
    });

    console.log(orderWithDistances);

    if (serveOrderWithSameDrone) {
      let groupOrders = orderWithDistances.reduce((acc, curr) => {
        if (acc[curr.customerId]) {
          acc[curr.customerId].push(curr);
        } else {
          acc[curr.customerId] = [curr];
        }

        return acc;
      }, {});

      console.log(groupOrders);

      groupOrders = Object.values(groupOrders).map((orderGroup) => {
        return orderGroup.reduce((acc, curr, index) => {
          if (index > 0) {
            return acc + curr.distance * 2;
          }

          return acc + curr.distance;
        }, 0);
      });

      return Math.max(...groupOrders);
    } else {
      return Math.max(...orderWithDistances.map((order) => order.distance));
    }
  }

  calculateDistance(location1, location2) {
    let x = location2.x - location1.x;
    let y = location2.y - location1.y;

    return Math.sqrt(x * x + y * y);
  }

  findNearestWarehouse(customerLocation) {
    let minDistance = Infinity;

    for (const warehouse of this.warehouses) {
      const distance = this.calculateDistance(
        customerLocation,
        warehouse.location
      );

      if (distance < minDistance) {
        minDistance = distance;
      }
    }

    return minDistance;
  }
}

class RendereHTML {
  constructor() {
    this.html = '';
  }

  addElement() {
    this.html += `
    <div>
      <h1>CREATE WAREHOUSE:</h1>
      <div>
        <div>
          <h2>Warehouse</h2>
        </div>
        <div>Coordinate X:</div>
        <input id="create-warehouse__x" type="text" />
        <div>Coordinate Y:</div>
        <input id="create-warehouse__y" type="text" />
      </div>
      <div>
        <button id="create-warehouse__button">Create</button>
      </div>
    </div>
    <div>
      <h1>CREATE CUSTOMER:</h1>
      <h2>Customer</h2>
      <div>Coordinates X:</div>
      <input id="coordinates-customer__x" type="text" />
      <div>Coordinates Y:</div>
      <input id="coordinates-customer__y" type="text" />
      <div>
        <button id="create-customer__button">Create</button>
      </div>
    </div>
    <div>
      <h1>CREATE ORDERS:</h1>
      <div>Number of orders:</div>
      <input id="orders" type="text" />
      <div>
        <button id="customer-order__button">Create</button>
      </div>
    <div>
      <h1>CREATE DRONES:</h1>
      <div>Number of drones:</div>
      <input id="drones" type="text" />
      <div>
        <button id="create-drone__button">Create</button>
      </div>
      <h1>CREATE TEST:</h1>
      <div>
        <button id="create-test__button">Create Test</button>
      </div>
    </div>
  `;
  }

  generateHTML() {
    return this.html;
  }
}

const renderer = new RendereHTML();
renderer.addElement();
const generatedHTML = renderer.generateHTML();
const renderedContent = document.getElementById('rendered-content');
renderedContent.innerHTML = generatedHTML;

class GenerateId {
  static uuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
      (
        c ^
        (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
      ).toString(16)
    );
  }
}

class AddWarehouse {
  constructor() {
    this.createWarehouseButton = document.getElementById(
      'create-warehouse__button'
    );
    this.newWarehouse = null;
    this.newWarehouses = [];

    this.createWarehouseButton.addEventListener('click', () => {
      this.handleCreateWarehouse();
    });
  }

  handleCreateWarehouse() {
    const warehouseCoordX = document.getElementById(
      'create-warehouse__x'
    ).value;
    const warehouseCoordY = document.getElementById(
      'create-warehouse__y'
    ).value;
    this.newWarehouse = new Warehouse(
      GenerateId.uuidv4(),
      warehouseCoordX,
      warehouseCoordY
    );
    console.log(this.newWarehouse);
    console.log('New warehouse created!');
    this.newWarehouses.push(this.newWarehouse);
    console.log(this.newWarehouses);
  }
}

const warehouses = [];
const addWarehouse = new AddWarehouse();
warehouses.push(addWarehouse);
console.log(addWarehouse);

class WarehouseManager extends AddWarehouse {
  warehouse1 = new Warehouse(1, 2, 2);
  warehouse2 = new Warehouse(2, 4, 6);
  defaultWarehouse = [warehouse1, warehouse2];
}

class AddDrone {
  constructor() {
    this.createDroneButton = document.getElementById('create-drone__button');
    this.numberOfDronesInput = document.getElementById('drones');
    this.newDrones = [];

    this.createDroneButton.addEventListener('click', () => {
      this.handleCreateDrones();
    });
  }

  handleCreateDrones() {
    const numberOfDrones = parseInt(this.numberOfDronesInput.value);

    for (let i = 0; i < numberOfDrones; i++) {
      const newDrone = new Drone(GenerateId.uuidv4());
      console.log(newDrone);
      this.newDrones.push(newDrone);
    }

    console.log('New drones created!');
    console.log(this.newDrones);
  }
}

const addDrones = new AddDrone();
console.log(addDrones.newDrones);

class AddOrders {
  constructor() {
    this.createOrderButton = document.getElementById('customer-order__button');
    this.numberOfOrdersInput = document.getElementById('orders');
    this.newOrders = [];

    this.createOrderButton.addEventListener('click', () => {
      this.handleCreateOrders();
    });
  }

  handleCreateOrders() {
    const numberOfOrders = parseInt(this.numberOfOrdersInput.value);

    for (let i = 0; i < numberOfOrders; i++) {
      const newOrder = new Order(GenerateId.uuidv4());
      console.log(newOrder);
      this.newOrders.push(newOrder);
    }

    console.log('New orders created!');
    console.log(this.newOrders);
  }
  getOrders() {
    return this.newOrders;
  }
}

const ordersList = new AddOrders();
console.log(ordersList.newOrders);

class AddCustomers {
  constructor() {
    this.createCustomerButton = document.getElementById(
      'create-customer__button'
    );
    this.newCustomer;

    this.createCustomerButton.addEventListener('click', () => {
      this.handleCreateCustomers();
    });
  }

  handleCreateCustomers() {
    const customerCoordsX = document.getElementById(
      'coordinates-customer__x'
    ).value;
    const customerCoordsY = document.getElementById(
      'coordinates-customer__y'
    ).value;
    this.newCustomer = new Customer(
      GenerateId.uuidv4(),
      customerCoordsX,
      customerCoordsY
    );
    console.log(this.newCustomer);
    console.log('New customer created!');
    console.log(this.newCustomers);
  }
}

const customersList = [];
const addCustomers = new AddCustomers();
customersList.push(addCustomers);
console.log(addCustomers.newCustomers, customersList);

// for reference:
const warehouse1 = new Warehouse(1, 2, 2);
const warehouse2 = new Warehouse(2, 4, 6);

const customer1 = new Customer(1, 1, 3);
const customer2 = new Customer(2, 5, 2);
const customer3 = new Customer(3, 8, 6);

const orders = [
  { customerId: 1, products: { bananas: 1, carrots: 2, milk: 2 } },
  { customerId: 1, products: { cucumber: 1, carrots: 2, eggs: 3 } },
  { customerId: 2, products: { milk: 1, cucumber: 2, onion: 3 } },
  { customerId: 3, products: { ham: 1, tomatoes: 2, milk: 3 } },
  { customerId: 3, products: { ham: 1, cheese: 2, bread: 3 } },
];
//----------------

const network = new DroneDeliveryNetwork(
  [warehouse1, warehouse2],
  [customer1, customer2, customer3],
  orders
);
const totalTime = network.calculateTotalTime();
console.log('Total Time:', totalTime);
